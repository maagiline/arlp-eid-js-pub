"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const IdCard_1 = require("./IdCard");
const MobileId_1 = require("./MobileId");
class default_1 {
    constructor(serviceUrl, overrideMocking = false) {
        if (!serviceUrl) {
            throw new Error('serviceUrl must be provided to invoke ArlpEid');
        }
        // Remove trailing slash
        this.serviceUrl = serviceUrl.replace(/\/$/, '');
        this.overrideMocking = overrideMocking;
        this.idCard = new IdCard_1.IdCard(this.serviceUrl, this.overrideMocking);
        this.mobileId = new MobileId_1.MobileId(this.serviceUrl, this.overrideMocking);
        this.authenticate = {
            idCard: {
                getPerson: (...args) => this.idCard.getPerson(...args),
            },
            mobileId: {
                getPerson: (...args) => this.mobileId.getPerson(...args),
                initiate: (...args) => this.mobileId.authInit(...args),
            },
        };
        this.sign = {
            idCard: {
                sign: (...args) => this.idCard.sign(...args),
            },
            mobileId: {
                checkStatus: (...args) => this.mobileId.signCheckStatus(...args),
                initiate: (...args) => this.mobileId.signInit(...args),
            },
        };
    }
}
exports.default = default_1;
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiaW5kZXguanMiLCJzb3VyY2VSb290IjoiIiwic291cmNlcyI6WyIuLi8uLi9zcmMvaW5kZXgudHMiXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6Ijs7QUFBQSxxQ0FBaUM7QUFDakMseUNBQXFDO0FBU3JDO0lBTUksWUFBWSxVQUFrQixFQUFFLGtCQUEyQixLQUFLO1FBQzVELElBQUksQ0FBQyxVQUFVLEVBQUU7WUFDYixNQUFNLElBQUksS0FBSyxDQUFDLCtDQUErQyxDQUFDLENBQUE7U0FDbkU7UUFFRCx3QkFBd0I7UUFDeEIsSUFBSSxDQUFDLFVBQVUsR0FBRyxVQUFVLENBQUMsT0FBTyxDQUFDLEtBQUssRUFBRSxFQUFFLENBQUMsQ0FBQTtRQUMvQyxJQUFJLENBQUMsZUFBZSxHQUFHLGVBQWUsQ0FBQTtRQUN0QyxJQUFJLENBQUMsTUFBTSxHQUFHLElBQUksZUFBTSxDQUFDLElBQUksQ0FBQyxVQUFVLEVBQUUsSUFBSSxDQUFDLGVBQWUsQ0FBQyxDQUFBO1FBQy9ELElBQUksQ0FBQyxRQUFRLEdBQUcsSUFBSSxtQkFBUSxDQUFDLElBQUksQ0FBQyxVQUFVLEVBQUUsSUFBSSxDQUFDLGVBQWUsQ0FBQyxDQUFBO1FBRW5FLElBQUksQ0FBQyxZQUFZLEdBQUc7WUFDaEIsTUFBTSxFQUFFO2dCQUNKLFNBQVMsRUFBRSxDQUFDLEdBQUcsSUFBSSxFQUFFLEVBQUUsQ0FBQyxJQUFJLENBQUMsTUFBTSxDQUFDLFNBQVMsQ0FBQyxHQUFHLElBQUksQ0FBQzthQUN6RDtZQUNELFFBQVEsRUFBRTtnQkFDTixTQUFTLEVBQUUsQ0FBQyxHQUFHLElBQUksRUFBRSxFQUFFLENBQUMsSUFBSSxDQUFDLFFBQVEsQ0FBQyxTQUFTLENBQUMsR0FBRyxJQUFJLENBQUM7Z0JBQ3hELFFBQVEsRUFBRSxDQUFDLEdBQUcsSUFBSSxFQUFFLEVBQUUsQ0FBQyxJQUFJLENBQUMsUUFBUSxDQUFDLFFBQVEsQ0FBQyxHQUFHLElBQUksQ0FBQzthQUN6RDtTQUNKLENBQUE7UUFFRCxJQUFJLENBQUMsSUFBSSxHQUFHO1lBQ1IsTUFBTSxFQUFFO2dCQUNKLElBQUksRUFBRSxDQUFDLEdBQUcsSUFBSSxFQUFFLEVBQUUsQ0FBQyxJQUFJLENBQUMsTUFBTSxDQUFDLElBQUksQ0FBQyxHQUFHLElBQUksQ0FBQzthQUMvQztZQUNELFFBQVEsRUFBRTtnQkFDTixXQUFXLEVBQUUsQ0FBQyxHQUFHLElBQUksRUFBRSxFQUFFLENBQUMsSUFBSSxDQUFDLFFBQVEsQ0FBQyxlQUFlLENBQUMsR0FBRyxJQUFJLENBQUM7Z0JBQ2hFLFFBQVEsRUFBRSxDQUFDLEdBQUcsSUFBSSxFQUFFLEVBQUUsQ0FBQyxJQUFJLENBQUMsUUFBUSxDQUFDLFFBQVEsQ0FBQyxHQUFHLElBQUksQ0FBQzthQUN6RDtTQUNKLENBQUE7SUFDTCxDQUFDO0NBcUJKO0FBekRELDRCQXlEQyJ9