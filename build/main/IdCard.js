"use strict";
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
const axios_1 = __importDefault(require("axios"));
const hwcrypto_js_1 = __importDefault(require("./utils/hwcrypto.js"));
class IdCard {
    constructor(serviceUrl, overrideMocking = false) {
        this.serviceUrl = serviceUrl;
        this.overrideMocking = overrideMocking;
    }
    sign(lang, containerId) {
        return new Promise((resolve, reject) => {
            const authReqUrl = `${this.serviceUrl}/authenticate/id-card`;
            const initReqUrl = `${this.serviceUrl}/sign/id-card/initiate`;
            const finReqUrl = `${this.serviceUrl}/sign/id-card/finalise`;
            if (this.overrideMocking) {
                hwcrypto_js_1.default().getCertificate({ filter: 'SIGN' }).then((certificate) => {
                    const authParams = { lang, overrideMocking: this.overrideMocking, certificate: certificate.hex };
                    const initParams = { lang, overrideMocking: this.overrideMocking, certificate: certificate.hex, containerId };
                    axios_1.default.post(authReqUrl, authParams, { withCredentials: true }).then((authResponse) => {
                        axios_1.default.post(initReqUrl, initParams, { withCredentials: true }).then((initResponse) => {
                            const hash = {
                                hex: initResponse.data.digest,
                                type: 'SHA-256',
                            };
                            hwcrypto_js_1.default().sign(certificate, hash, { lang: 'et' }).then((signature) => {
                                const finParams = { overrideMocking: this.overrideMocking, signatureId: initResponse.data.signatureId, value: signature.hex, containerId };
                                axios_1.default.post(finReqUrl, finParams, { withCredentials: true }).then(() => {
                                    resolve(authResponse.data);
                                }, (finalizeError) => { reject(finalizeError); });
                            }, (hwcSignError) => { reject(hwcSignError); });
                        }, (initError) => { reject(initError); });
                    }, (authError) => { reject(authError); });
                }, (hwcCertError) => { reject(hwcCertError); });
            }
            else {
                const authParams = { lang, overrideMocking: this.overrideMocking, certificate: 'HEX_DEMO' };
                const initParams = { lang, overrideMocking: this.overrideMocking, certificate: 'HEX_DEMO', containerId };
                axios_1.default.post(authReqUrl, authParams, { withCredentials: true }).then((authResponse) => {
                    axios_1.default.post(initReqUrl, initParams, { withCredentials: true }).then((initResponse) => {
                        const finParams = { overrideMocking: this.overrideMocking, signatureId: initResponse.data.signatureId, value: 'HEX_DEMO', containerId };
                        axios_1.default.post(finReqUrl, finParams, { withCredentials: true }).then(() => {
                            resolve(authResponse.data);
                        }, (finalizeError) => { reject(finalizeError); });
                    }, (initError) => { reject(initError); });
                }, (authError) => { reject(authError); });
            }
        });
    }
    getPerson(lang) {
        return new Promise((resolve, reject) => {
            const reqUrl = `${this.serviceUrl}/authenticate/id-card`;
            if (this.overrideMocking) {
                hwcrypto_js_1.default().getCertificate({ filter: 'AUTH' }).then((certificate) => {
                    const params = { lang, overrideMocking: this.overrideMocking, certificate: certificate.hex };
                    axios_1.default.post(reqUrl, params, { withCredentials: true }).then((response) => {
                        const hash = {
                            hex: response.data.hmac,
                            type: 'SHA-256',
                        };
                        hwcrypto_js_1.default().sign(certificate, hash).then(() => {
                            resolve(response.data);
                        }, (hwcSignError) => { reject(hwcSignError); });
                    }, (authError) => { reject(authError); });
                }, (hwcCertError) => { reject(hwcCertError); });
            }
            else {
                const params = { lang, overrideMocking: this.overrideMocking, certificate: 'HEX_DEMO' };
                axios_1.default.post(reqUrl, params, { withCredentials: true }).then((response) => {
                    resolve(response.data);
                }, (authError) => { reject(authError); });
            }
        });
    }
}
exports.IdCard = IdCard;
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiSWRDYXJkLmpzIiwic291cmNlUm9vdCI6IiIsInNvdXJjZXMiOlsiLi4vLi4vc3JjL0lkQ2FyZC50cyJdLCJuYW1lcyI6W10sIm1hcHBpbmdzIjoiOzs7OztBQUFBLGtEQUF5QjtBQUl6QixzRUFBMEM7QUFPMUMsTUFBYSxNQUFNO0lBSWYsWUFBWSxVQUFrQixFQUFFLGtCQUEyQixLQUFLO1FBQzVELElBQUksQ0FBQyxVQUFVLEdBQUcsVUFBVSxDQUFBO1FBQzVCLElBQUksQ0FBQyxlQUFlLEdBQUcsZUFBZSxDQUFBO0lBQzFDLENBQUM7SUFFTSxJQUFJLENBQUMsSUFBWSxFQUFFLFdBQW1CO1FBQ3pDLE9BQU8sSUFBSSxPQUFPLENBQUMsQ0FBQyxPQUFPLEVBQUUsTUFBTSxFQUFFLEVBQUU7WUFDbkMsTUFBTSxVQUFVLEdBQUcsR0FBRyxJQUFJLENBQUMsVUFBVSx1QkFBdUIsQ0FBQTtZQUM1RCxNQUFNLFVBQVUsR0FBRyxHQUFHLElBQUksQ0FBQyxVQUFVLHdCQUF3QixDQUFBO1lBQzdELE1BQU0sU0FBUyxHQUFHLEdBQUcsSUFBSSxDQUFDLFVBQVUsd0JBQXdCLENBQUE7WUFFNUQsSUFBSSxJQUFJLENBQUMsZUFBZSxFQUFFO2dCQUN0QixxQkFBUSxFQUFFLENBQUMsY0FBYyxDQUFDLEVBQUUsTUFBTSxFQUFFLE1BQU0sRUFBRSxDQUFDLENBQUMsSUFBSSxDQUFDLENBQUMsV0FBVyxFQUFFLEVBQUU7b0JBQy9ELE1BQU0sVUFBVSxHQUFHLEVBQUUsSUFBSSxFQUFFLGVBQWUsRUFBRSxJQUFJLENBQUMsZUFBZSxFQUFFLFdBQVcsRUFBRSxXQUFXLENBQUMsR0FBRyxFQUFFLENBQUE7b0JBQ2hHLE1BQU0sVUFBVSxHQUFHLEVBQUUsSUFBSSxFQUFFLGVBQWUsRUFBRSxJQUFJLENBQUMsZUFBZSxFQUFFLFdBQVcsRUFBRSxXQUFXLENBQUMsR0FBRyxFQUFFLFdBQVcsRUFBRSxDQUFBO29CQUU3RyxlQUFLLENBQUMsSUFBSSxDQUFDLFVBQVUsRUFBRSxVQUFVLEVBQUUsRUFBQyxlQUFlLEVBQUUsSUFBSSxFQUFDLENBQUMsQ0FBQyxJQUFJLENBQUMsQ0FBQyxZQUFZLEVBQUUsRUFBRTt3QkFDOUUsZUFBSyxDQUFDLElBQUksQ0FBQyxVQUFVLEVBQUUsVUFBVSxFQUFFLEVBQUMsZUFBZSxFQUFFLElBQUksRUFBQyxDQUFDLENBQUMsSUFBSSxDQUFDLENBQUMsWUFBWSxFQUFFLEVBQUU7NEJBQzlFLE1BQU0sSUFBSSxHQUFHO2dDQUNULEdBQUcsRUFBRSxZQUFZLENBQUMsSUFBSSxDQUFDLE1BQU07Z0NBQzdCLElBQUksRUFBRSxTQUFTOzZCQUNsQixDQUFBOzRCQUNELHFCQUFRLEVBQUUsQ0FBQyxJQUFJLENBQUMsV0FBVyxFQUFFLElBQUksRUFBRSxFQUFFLElBQUksRUFBRSxJQUFJLEVBQUUsQ0FBQyxDQUFDLElBQUksQ0FBQyxDQUFDLFNBQVMsRUFBRSxFQUFFO2dDQUNsRSxNQUFNLFNBQVMsR0FBRyxFQUFFLGVBQWUsRUFBRSxJQUFJLENBQUMsZUFBZSxFQUFFLFdBQVcsRUFBRSxZQUFZLENBQUMsSUFBSSxDQUFDLFdBQVcsRUFBRSxLQUFLLEVBQUUsU0FBUyxDQUFDLEdBQUcsRUFBRSxXQUFXLEVBQUUsQ0FBQTtnQ0FFMUksZUFBSyxDQUFDLElBQUksQ0FBQyxTQUFTLEVBQUUsU0FBUyxFQUFFLEVBQUMsZUFBZSxFQUFFLElBQUksRUFBQyxDQUFDLENBQUMsSUFBSSxDQUFDLEdBQUcsRUFBRTtvQ0FDaEUsT0FBTyxDQUFDLFlBQVksQ0FBQyxJQUFJLENBQUMsQ0FBQTtnQ0FDOUIsQ0FBQyxFQUFFLENBQUMsYUFBYSxFQUFFLEVBQUUsR0FBRyxNQUFNLENBQUMsYUFBYSxDQUFDLENBQUEsQ0FBQyxDQUFDLENBQUMsQ0FBQTs0QkFDcEQsQ0FBQyxFQUFFLENBQUMsWUFBWSxFQUFFLEVBQUUsR0FBRyxNQUFNLENBQUMsWUFBWSxDQUFDLENBQUEsQ0FBQyxDQUFDLENBQUMsQ0FBQTt3QkFFbEQsQ0FBQyxFQUFFLENBQUMsU0FBUyxFQUFFLEVBQUUsR0FBRyxNQUFNLENBQUMsU0FBUyxDQUFDLENBQUEsQ0FBQyxDQUFDLENBQUMsQ0FBQTtvQkFDNUMsQ0FBQyxFQUFFLENBQUMsU0FBUyxFQUFFLEVBQUUsR0FBRyxNQUFNLENBQUMsU0FBUyxDQUFDLENBQUEsQ0FBQyxDQUFDLENBQUMsQ0FBQTtnQkFDNUMsQ0FBQyxFQUFFLENBQUMsWUFBWSxFQUFFLEVBQUUsR0FBRyxNQUFNLENBQUMsWUFBWSxDQUFDLENBQUEsQ0FBQyxDQUFDLENBQUMsQ0FBQTthQUNqRDtpQkFBTTtnQkFDSCxNQUFNLFVBQVUsR0FBRyxFQUFFLElBQUksRUFBRSxlQUFlLEVBQUUsSUFBSSxDQUFDLGVBQWUsRUFBRSxXQUFXLEVBQUUsVUFBVSxFQUFFLENBQUE7Z0JBQzNGLE1BQU0sVUFBVSxHQUFHLEVBQUUsSUFBSSxFQUFFLGVBQWUsRUFBRSxJQUFJLENBQUMsZUFBZSxFQUFFLFdBQVcsRUFBRSxVQUFVLEVBQUUsV0FBVyxFQUFFLENBQUE7Z0JBRXhHLGVBQUssQ0FBQyxJQUFJLENBQUMsVUFBVSxFQUFFLFVBQVUsRUFBRSxFQUFDLGVBQWUsRUFBRSxJQUFJLEVBQUMsQ0FBQyxDQUFDLElBQUksQ0FBQyxDQUFDLFlBQVksRUFBRSxFQUFFO29CQUM5RSxlQUFLLENBQUMsSUFBSSxDQUFDLFVBQVUsRUFBRSxVQUFVLEVBQUUsRUFBQyxlQUFlLEVBQUUsSUFBSSxFQUFDLENBQUMsQ0FBQyxJQUFJLENBQUMsQ0FBQyxZQUFZLEVBQUUsRUFBRTt3QkFDOUUsTUFBTSxTQUFTLEdBQUcsRUFBRSxlQUFlLEVBQUUsSUFBSSxDQUFDLGVBQWUsRUFBRSxXQUFXLEVBQUUsWUFBWSxDQUFDLElBQUksQ0FBQyxXQUFXLEVBQUUsS0FBSyxFQUFFLFVBQVUsRUFBRSxXQUFXLEVBQUUsQ0FBQTt3QkFFdkksZUFBSyxDQUFDLElBQUksQ0FBQyxTQUFTLEVBQUUsU0FBUyxFQUFFLEVBQUMsZUFBZSxFQUFFLElBQUksRUFBQyxDQUFDLENBQUMsSUFBSSxDQUFDLEdBQUcsRUFBRTs0QkFDaEUsT0FBTyxDQUFDLFlBQVksQ0FBQyxJQUFJLENBQUMsQ0FBQTt3QkFDOUIsQ0FBQyxFQUFFLENBQUMsYUFBYSxFQUFFLEVBQUUsR0FBRyxNQUFNLENBQUMsYUFBYSxDQUFDLENBQUEsQ0FBQyxDQUFDLENBQUMsQ0FBQTtvQkFDcEQsQ0FBQyxFQUFFLENBQUMsU0FBUyxFQUFFLEVBQUUsR0FBRyxNQUFNLENBQUMsU0FBUyxDQUFDLENBQUEsQ0FBQyxDQUFDLENBQUMsQ0FBQTtnQkFDNUMsQ0FBQyxFQUFFLENBQUMsU0FBUyxFQUFFLEVBQUUsR0FBRyxNQUFNLENBQUMsU0FBUyxDQUFDLENBQUEsQ0FBQyxDQUFDLENBQUMsQ0FBQTthQUMzQztRQUNMLENBQUMsQ0FBQyxDQUFBO0lBQ04sQ0FBQztJQUVNLFNBQVMsQ0FBQyxJQUFZO1FBQ3pCLE9BQU8sSUFBSSxPQUFPLENBQUMsQ0FBQyxPQUFPLEVBQUUsTUFBTSxFQUFFLEVBQUU7WUFDbkMsTUFBTSxNQUFNLEdBQUcsR0FBRyxJQUFJLENBQUMsVUFBVSx1QkFBdUIsQ0FBQTtZQUN4RCxJQUFJLElBQUksQ0FBQyxlQUFlLEVBQUU7Z0JBQ3RCLHFCQUFRLEVBQUUsQ0FBQyxjQUFjLENBQUMsRUFBQyxNQUFNLEVBQUUsTUFBTSxFQUFDLENBQUMsQ0FBQyxJQUFJLENBQUMsQ0FBQyxXQUFXLEVBQUUsRUFBRTtvQkFDN0QsTUFBTSxNQUFNLEdBQUcsRUFBRSxJQUFJLEVBQUUsZUFBZSxFQUFFLElBQUksQ0FBQyxlQUFlLEVBQUUsV0FBVyxFQUFFLFdBQVcsQ0FBQyxHQUFHLEVBQUUsQ0FBQTtvQkFFNUYsZUFBSyxDQUFDLElBQUksQ0FBQyxNQUFNLEVBQUUsTUFBTSxFQUFFLEVBQUMsZUFBZSxFQUFFLElBQUksRUFBQyxDQUFDLENBQUMsSUFBSSxDQUFDLENBQUMsUUFBUSxFQUFFLEVBQUU7d0JBQ2xFLE1BQU0sSUFBSSxHQUFHOzRCQUNULEdBQUcsRUFBRSxRQUFRLENBQUMsSUFBSSxDQUFDLElBQUk7NEJBQ3ZCLElBQUksRUFBRSxTQUFTO3lCQUNsQixDQUFBO3dCQUNELHFCQUFRLEVBQUUsQ0FBQyxJQUFJLENBQUMsV0FBVyxFQUFFLElBQUksQ0FBQyxDQUFDLElBQUksQ0FBQyxHQUFHLEVBQUU7NEJBQ3pDLE9BQU8sQ0FBQyxRQUFRLENBQUMsSUFBSSxDQUFDLENBQUE7d0JBQzFCLENBQUMsRUFBRSxDQUFDLFlBQVksRUFBRSxFQUFFLEdBQUcsTUFBTSxDQUFDLFlBQVksQ0FBQyxDQUFBLENBQUMsQ0FBQyxDQUFDLENBQUE7b0JBQ2xELENBQUMsRUFBRSxDQUFDLFNBQVMsRUFBRSxFQUFFLEdBQUcsTUFBTSxDQUFDLFNBQVMsQ0FBQyxDQUFBLENBQUMsQ0FBQyxDQUFDLENBQUE7Z0JBQzVDLENBQUMsRUFBRSxDQUFDLFlBQVksRUFBRSxFQUFFLEdBQUcsTUFBTSxDQUFDLFlBQVksQ0FBQyxDQUFBLENBQUMsQ0FBQyxDQUFDLENBQUE7YUFDakQ7aUJBQU07Z0JBQ0gsTUFBTSxNQUFNLEdBQUcsRUFBRSxJQUFJLEVBQUUsZUFBZSxFQUFFLElBQUksQ0FBQyxlQUFlLEVBQUUsV0FBVyxFQUFFLFVBQVUsRUFBRSxDQUFBO2dCQUV2RixlQUFLLENBQUMsSUFBSSxDQUFDLE1BQU0sRUFBRSxNQUFNLEVBQUUsRUFBQyxlQUFlLEVBQUUsSUFBSSxFQUFDLENBQUMsQ0FBQyxJQUFJLENBQUMsQ0FBQyxRQUFRLEVBQUUsRUFBRTtvQkFDbEUsT0FBTyxDQUFDLFFBQVEsQ0FBQyxJQUFJLENBQUMsQ0FBQTtnQkFDMUIsQ0FBQyxFQUFFLENBQUMsU0FBUyxFQUFFLEVBQUUsR0FBRyxNQUFNLENBQUMsU0FBUyxDQUFDLENBQUEsQ0FBQyxDQUFDLENBQUMsQ0FBQTthQUMzQztRQUNMLENBQUMsQ0FBQyxDQUFBO0lBQ04sQ0FBQztDQUNKO0FBaEZELHdCQWdGQyJ9