"use strict";
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
const axios_1 = __importDefault(require("axios"));
class MobileId {
    constructor(serviceUrl, overrideMocking = false) {
        this.serviceUrl = serviceUrl;
        this.overrideMocking = overrideMocking;
    }
    authInit(lang, ssn, phone) {
        return new Promise((resolve, reject) => {
            const reqUrl = `${this.serviceUrl}/authenticate/mobile-id/initiate`;
            const params = { lang, ssn, phone, overrideMocking: this.overrideMocking };
            // Notify dev of possibility to specify returned user
            // tslint-disable-next-line no-console
            console.log(`Started mobile ID authentication in demo mode
                [${lang}][${ssn}][${phone}]. To
                specify which identity should be returned, make a
                request to ${this.serviceUrl}/demo/authenticatable
                with params firstName, lastName, ssn`);
            axios_1.default.post(reqUrl, params, { withCredentials: true })
                .then(response => {
                resolve(response.data.challengeId);
            }, (error) => reject(error));
        });
    }
    getPerson(lang) {
        return new Promise((resolve, reject) => {
            let reqUrl = `${this.serviceUrl}/authenticate/mobile-id/check`;
            const params = { lang, overrideMocking: this.overrideMocking };
            axios_1.default.post(reqUrl, params, { withCredentials: true })
                .then((response) => {
                if (response.data.finished)
                    resolve(response.data);
                else
                    setTimeout(() => this.getPerson(lang).then((responseData) => resolve(responseData)), 1000);
            }, (error) => reject(error));
        });
    }
    signInit(lang, containerId, ssn, phone) {
        return new Promise((resolve, reject) => {
            const reqUrl = `${this.serviceUrl}/sign/mobile-id/initiate`;
            const params = { lang, containerId, ssn, phone, overrideMocking: this.overrideMocking };
            // Notify dev of possibility to specify returned user
            console.log(`Started mobile ID signing in demo mode
                [${lang}][${containerId}][${ssn}][${phone}]. To
                specify which identity should be returned, make a
                request to ${this.serviceUrl}/demo/authenticatable
                with params firstName, lastName, ssn`);
            axios_1.default.post(reqUrl, params, { withCredentials: true })
                .then((response) => {
                resolve(response.data.challengeId);
            }, (error) => reject(error));
        });
    }
    signCheckStatus(lang) {
        return new Promise((resolve, reject) => {
            const reqUrl = `${this.serviceUrl}/sign/mobile-id/check`;
            const params = { lang, overrideMocking: this.overrideMocking };
            axios_1.default.post(reqUrl, params, { withCredentials: true }).then((response) => {
                if (response.data.finished) {
                    resolve(response.data);
                }
                else {
                    setTimeout(() => this.signCheckStatus(lang).then((responseData) => resolve(responseData)), 1000);
                }
            }, (error) => reject(error));
        });
    }
}
exports.MobileId = MobileId;
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiTW9iaWxlSWQuanMiLCJzb3VyY2VSb290IjoiIiwic291cmNlcyI6WyIuLi8uLi9zcmMvTW9iaWxlSWQudHMiXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6Ijs7Ozs7QUFBQSxrREFBeUI7QUFjekIsTUFBYSxRQUFRO0lBSWpCLFlBQVksVUFBa0IsRUFBRSxrQkFBMkIsS0FBSztRQUM1RCxJQUFJLENBQUMsVUFBVSxHQUFHLFVBQVUsQ0FBQTtRQUM1QixJQUFJLENBQUMsZUFBZSxHQUFHLGVBQWUsQ0FBQTtJQUMxQyxDQUFDO0lBRU0sUUFBUSxDQUFDLElBQVksRUFBRSxHQUFXLEVBQUUsS0FBYTtRQUNwRCxPQUFPLElBQUksT0FBTyxDQUFDLENBQUMsT0FBTyxFQUFFLE1BQU0sRUFBRSxFQUFFO1lBQ25DLE1BQU0sTUFBTSxHQUFHLEdBQUcsSUFBSSxDQUFDLFVBQVUsa0NBQWtDLENBQUE7WUFDbkUsTUFBTSxNQUFNLEdBQUcsRUFBRSxJQUFJLEVBQUUsR0FBRyxFQUFFLEtBQUssRUFBRSxlQUFlLEVBQUUsSUFBSSxDQUFDLGVBQWUsRUFBRSxDQUFBO1lBRXRGLHFEQUFxRDtZQUN6QyxzQ0FBc0M7WUFDdEMsT0FBTyxDQUFDLEdBQUcsQ0FBQzttQkFDTCxJQUFJLEtBQUssR0FBRyxLQUFLLEtBQUs7OzZCQUVaLElBQUksQ0FBQyxVQUFVO3FEQUNTLENBQUMsQ0FBQTtZQUUxQyxlQUFLLENBQUMsSUFBSSxDQUFDLE1BQU0sRUFBRSxNQUFNLEVBQUUsRUFBQyxlQUFlLEVBQUUsSUFBSSxFQUFDLENBQUM7aUJBQzlDLElBQUksQ0FBQyxRQUFRLENBQUMsRUFBRTtnQkFDYixPQUFPLENBQUMsUUFBUSxDQUFDLElBQUksQ0FBQyxXQUFXLENBQUMsQ0FBQTtZQUN0QyxDQUFDLEVBQUUsQ0FBQyxLQUFLLEVBQUUsRUFBRSxDQUFDLE1BQU0sQ0FBQyxLQUFLLENBQUMsQ0FBQyxDQUFBO1FBQ3BDLENBQUMsQ0FBQyxDQUFBO0lBQ04sQ0FBQztJQUVNLFNBQVMsQ0FBQyxJQUFZO1FBQ3pCLE9BQU8sSUFBSSxPQUFPLENBQUMsQ0FBQyxPQUFPLEVBQUUsTUFBTSxFQUFFLEVBQUU7WUFDbkMsSUFBSSxNQUFNLEdBQUcsR0FBRyxJQUFJLENBQUMsVUFBVSwrQkFBK0IsQ0FBQTtZQUM5RCxNQUFNLE1BQU0sR0FBRyxFQUFFLElBQUksRUFBRSxlQUFlLEVBQUUsSUFBSSxDQUFDLGVBQWUsRUFBRSxDQUFBO1lBRTlELGVBQUssQ0FBQyxJQUFJLENBQUMsTUFBTSxFQUFFLE1BQU0sRUFBRSxFQUFDLGVBQWUsRUFBRSxJQUFJLEVBQUMsQ0FBQztpQkFDOUMsSUFBSSxDQUFDLENBQUMsUUFBUSxFQUFFLEVBQUU7Z0JBQ2YsSUFBSSxRQUFRLENBQUMsSUFBSSxDQUFDLFFBQVE7b0JBQUUsT0FBTyxDQUFDLFFBQVEsQ0FBQyxJQUFJLENBQUMsQ0FBQTs7b0JBQzdDLFVBQVUsQ0FBQyxHQUFHLEVBQUUsQ0FBQyxJQUFJLENBQUMsU0FBUyxDQUFDLElBQUksQ0FBQyxDQUFDLElBQUksQ0FBQyxDQUFDLFlBQVksRUFBRSxFQUFFLENBQUMsT0FBTyxDQUFDLFlBQVksQ0FBQyxDQUFDLEVBQUUsSUFBSSxDQUFDLENBQUE7WUFDbkcsQ0FBQyxFQUFFLENBQUMsS0FBSyxFQUFFLEVBQUUsQ0FBQyxNQUFNLENBQUMsS0FBSyxDQUFDLENBQUMsQ0FBQTtRQUNwQyxDQUFDLENBQUMsQ0FBQTtJQUNOLENBQUM7SUFFTSxRQUFRLENBQUMsSUFBWSxFQUFFLFdBQW1CLEVBQUUsR0FBVyxFQUFFLEtBQWE7UUFDekUsT0FBTyxJQUFJLE9BQU8sQ0FBQyxDQUFDLE9BQU8sRUFBRSxNQUFNLEVBQUUsRUFBRTtZQUNuQyxNQUFNLE1BQU0sR0FBRyxHQUFHLElBQUksQ0FBQyxVQUFVLDBCQUEwQixDQUFBO1lBQzNELE1BQU0sTUFBTSxHQUFHLEVBQUUsSUFBSSxFQUFFLFdBQVcsRUFBRSxHQUFHLEVBQUUsS0FBSyxFQUFFLGVBQWUsRUFBRSxJQUFJLENBQUMsZUFBZSxFQUFFLENBQUE7WUFFdkYscURBQXFEO1lBQ3JELE9BQU8sQ0FBQyxHQUFHLENBQUM7bUJBQ0wsSUFBSSxLQUFLLFdBQVcsS0FBSyxHQUFHLEtBQUssS0FBSzs7NkJBRTVCLElBQUksQ0FBQyxVQUFVO3FEQUNTLENBQUMsQ0FBQTtZQUUxQyxlQUFLLENBQUMsSUFBSSxDQUFDLE1BQU0sRUFBRSxNQUFNLEVBQUUsRUFBQyxlQUFlLEVBQUUsSUFBSSxFQUFDLENBQUM7aUJBQzlDLElBQUksQ0FBQyxDQUFDLFFBQVEsRUFBRSxFQUFFO2dCQUNmLE9BQU8sQ0FBQyxRQUFRLENBQUMsSUFBSSxDQUFDLFdBQVcsQ0FBQyxDQUFBO1lBQ3RDLENBQUMsRUFBRSxDQUFDLEtBQUssRUFBRSxFQUFFLENBQUMsTUFBTSxDQUFDLEtBQUssQ0FBQyxDQUFDLENBQUE7UUFDcEMsQ0FBQyxDQUFDLENBQUE7SUFDTixDQUFDO0lBRU0sZUFBZSxDQUFDLElBQVk7UUFDL0IsT0FBTyxJQUFJLE9BQU8sQ0FBQyxDQUFDLE9BQU8sRUFBRSxNQUFNLEVBQUUsRUFBRTtZQUNuQyxNQUFNLE1BQU0sR0FBRyxHQUFHLElBQUksQ0FBQyxVQUFVLHVCQUF1QixDQUFBO1lBQ3hELE1BQU0sTUFBTSxHQUFHLEVBQUUsSUFBSSxFQUFFLGVBQWUsRUFBRSxJQUFJLENBQUMsZUFBZSxFQUFFLENBQUE7WUFFOUQsZUFBSyxDQUFDLElBQUksQ0FBQyxNQUFNLEVBQUUsTUFBTSxFQUFFLEVBQUMsZUFBZSxFQUFFLElBQUksRUFBQyxDQUFDLENBQUMsSUFBSSxDQUFDLENBQUMsUUFBUSxFQUFFLEVBQUU7Z0JBQ2xFLElBQUksUUFBUSxDQUFDLElBQUksQ0FBQyxRQUFRLEVBQUU7b0JBQ3hCLE9BQU8sQ0FBQyxRQUFRLENBQUMsSUFBSSxDQUFDLENBQUE7aUJBQ3pCO3FCQUFNO29CQUNILFVBQVUsQ0FBQyxHQUFHLEVBQUUsQ0FBQyxJQUFJLENBQUMsZUFBZSxDQUFDLElBQUksQ0FBQyxDQUFDLElBQUksQ0FBQyxDQUFDLFlBQVksRUFBRSxFQUFFLENBQUMsT0FBTyxDQUFDLFlBQVksQ0FBQyxDQUFDLEVBQUUsSUFBSSxDQUFDLENBQUE7aUJBQ25HO1lBQ0wsQ0FBQyxFQUFFLENBQUMsS0FBSyxFQUFFLEVBQUUsQ0FBQyxNQUFNLENBQUMsS0FBSyxDQUFDLENBQUMsQ0FBQTtRQUNoQyxDQUFDLENBQUMsQ0FBQTtJQUNOLENBQUM7Q0FDSjtBQTNFRCw0QkEyRUMifQ==