# arlpEid JS
A JS library for authenticating and signing with the Estonian ID card and Mobile ID. Currently, the package supports signing by ID card. Still to implement:

- Authentication by ID card
- Authentication by Mobile ID
- Signing by Mobile ID

To install:
`npm install https://bitbucket.org/maagiline/arlp-eid-js/ --save`
## Demo mode
Enabling demo mode allows to use this library without access to an actual Estonian ID card. Demo mode should never be enabled in production.

In demo mode, no signature is actually given. Whenever signing is initiated, it is considered to be successful.

When in demo mode, you may specify who the arlpEid service will return as the authenticated user:
```
curl -X POST \
  https://arlp-eid.maagiline.ee/demo/authenticatable \
  -H 'Content-Type: application/json' \
  -d '{
	"firstName": "Kimo",
	"lastName": "Kerr",
	"ssn": "39104090041"
}'
```
## Examples
### Initialize
```
import ArlpEid from 'arlp-eid'
const arlpEidUrl = 'https://arlp-eid.maagiline.ee'
const isDemo = true
const arlpEid = new ArlpEid(arlpEidUrl, isDemo)
```
### Sign by ID card
```
const lang = 'et'
const containerId = 1
arlpEid.sign.idCard.sign(lang, containerId).then(() => {
    console.log('Signing was successful')
}).catch(e => {
    console.log('Signing failed', e)
})
```
### Authenticate by ID card
```
const lang = 'et'
arlpEid.authenticate.idCard.getPerson(lang).then((personData) => {
    console.log('Auth was successful. ', personData)
    // Auth was successful. {"firstName":"Kimo","lastName":"Kerr","socialSecurityNumber":"39104090041","timestamp":1543426131,"hmac":"243fe31badfa2e4e4d703866017b910dff37fb96815f573a2506c21c04c97ad7ab332040e19383a4ceec022be8e50eb461fad3be11af0d3d6821048585f37238"}
}).catch(e => {
    console.log('Auth failed', e)
})
```
## Workflow
Estonian ID signing is done via containers. Container wraps the original document in a file that can be signed. In the ARLP system, we need to keep containers for each document. When a signature is added, the container file will be overwritten with a new container, this one containing the new signature.

## Workflow: ID card authenticate
Below is a scheme of the workflow of ID card authentication. Areas with black color are areas that need to be integrated with / implemented in the ARLP system. Areas in gray color are those handled by the ArlpEID JS package and ArlpEID back-end.

For hmac validation, a secret string key will be provided. On the demo server (arlp-eid.maagiline.ee), the key is the following: `E)H@MbQeThWmZq4t7w!z%C*F-JaNdRfUjXn2r5u8x/A?D(G+KbPeShVkYp3s6v9y`

![ID card workflow](./docs/ID-card-auth.png)

## Workflow: ID card sign
Below is a scheme of the workflow of ID card signing. Areas with black color are areas that need to be integrated with / implemented in the ARLP system. Areas in gray color are those handled by the ArlpEID JS package and ArlpEID back-end.

![ID card workflow](./docs/ID-card-sign.png)

## Checking hash
When sends their authentication data, we must ensure that this data was approved by the arlpEid server. We do this by validating the hmac that was sent from the arlpEid server:
```
// The data sent by client
$personData = {"firstName":"Kimo","lastName":"Kerr","socialSecurityNumber":"39104090041","timestamp":1543426131,"hmac":"243fe31badfa2e4e4d703866017b910dff37fb96815f573a2506c21c04c97ad7ab332040e19383a4ceec022be8e50eb461fad3be11af0d3d6821048585f37238"}

// Concat values of firstName, lastName, socialSecurityNumber, timestamp: KimoKerr391040900411543426131
$personDataConcat = 

create hmac using key and sha512: $soughtHash = hash_hmac('sha512','KimoKerr391040900411543426131','E)H@MbQeThWmZq4t7w!z%C*F-JaNdRfUjXn2r5u8x/A?D(G+KbPeShVkYp3s6v9y')

Check hmac against the one sent by client, using hash_equals: $clientHashIsValid = hash_equals($soughtHash, $clientHash)
```

## Testing this package
To test this package, maagiline/arlp-eid-js-test repo is available, which provides a html+JS front-end for testing this package in conjunction with the arlpEid back-end.
