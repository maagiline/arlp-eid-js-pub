import axios from 'axios'
import IidCardGetPerson from './types/IidCardGetPerson'
import IidCardSign from './types/IidCardSign'
import IpersonData from './types/IpersonData'
import hwcrypto from './utils/hwcrypto.js'

interface IIdCard {
    sign: IidCardSign,
    getPerson: IidCardGetPerson,
}

export class IdCard implements IIdCard {
    private serviceUrl: string
    private overrideMocking: boolean

    constructor(serviceUrl: string, overrideMocking: boolean = false) {
        this.serviceUrl = serviceUrl
        this.overrideMocking = overrideMocking
    }

    public sign(lang: string, containerId: number): Promise<void> {
        return new Promise((resolve, reject) => {
            const authReqUrl = `${this.serviceUrl}/authenticate/id-card`
            const initReqUrl = `${this.serviceUrl}/sign/id-card/initiate`
            const finReqUrl = `${this.serviceUrl}/sign/id-card/finalise`

            if (this.overrideMocking) {
                hwcrypto().getCertificate({ filter: 'SIGN' }).then((certificate) => {
                    const authParams = { lang, overrideMocking: this.overrideMocking, certificate: certificate.hex }
                    const initParams = { lang, overrideMocking: this.overrideMocking, certificate: certificate.hex, containerId }

                    axios.post(authReqUrl, authParams, {withCredentials: true}).then((authResponse) => {
                        axios.post(initReqUrl, initParams, {withCredentials: true}).then((initResponse) => {
                            const hash = {
                                hex: initResponse.data.digest,
                                type: 'SHA-256',
                            }
                            hwcrypto().sign(certificate, hash, { lang: 'et' }).then((signature) => {
                                const finParams = { overrideMocking: this.overrideMocking, signatureId: initResponse.data.signatureId, value: signature.hex, containerId }

                                axios.post(finReqUrl, finParams, {withCredentials: true}).then(() => {
                                    resolve(authResponse.data)
                                }, (finalizeError) => { reject(finalizeError) })
                            }, (hwcSignError) => { reject(hwcSignError) })

                        }, (initError) => { reject(initError) })
                    }, (authError) => { reject(authError) })
                }, (hwcCertError) => { reject(hwcCertError) })
            } else {
                const authParams = { lang, overrideMocking: this.overrideMocking, certificate: 'HEX_DEMO' }
                const initParams = { lang, overrideMocking: this.overrideMocking, certificate: 'HEX_DEMO', containerId }

                axios.post(authReqUrl, authParams, {withCredentials: true}).then((authResponse) => {
                    axios.post(initReqUrl, initParams, {withCredentials: true}).then((initResponse) => {
                        const finParams = { overrideMocking: this.overrideMocking, signatureId: initResponse.data.signatureId, value: 'HEX_DEMO', containerId }

                        axios.post(finReqUrl, finParams, {withCredentials: true}).then(() => {
                            resolve(authResponse.data)
                        }, (finalizeError) => { reject(finalizeError) })
                    }, (initError) => { reject(initError) })
                }, (authError) => { reject(authError) })
            }
        })
    }

    public getPerson(lang: string): Promise<IpersonData> {
        return new Promise((resolve, reject) => {
            const reqUrl = `${this.serviceUrl}/authenticate/id-card`
            if (this.overrideMocking) {
                hwcrypto().getCertificate({filter: 'AUTH'}).then((certificate) => {
                    const params = { lang, overrideMocking: this.overrideMocking, certificate: certificate.hex }

                    axios.post(reqUrl, params, {withCredentials: true}).then((response) => {
                        const hash = {
                            hex: response.data.hmac,
                            type: 'SHA-256',
                        }
                        hwcrypto().sign(certificate, hash).then(() => {
                            resolve(response.data)
                        }, (hwcSignError) => { reject(hwcSignError) })
                    }, (authError) => { reject(authError) })
                }, (hwcCertError) => { reject(hwcCertError) })
            } else {
                const params = { lang, overrideMocking: this.overrideMocking, certificate: 'HEX_DEMO' }

                axios.post(reqUrl, params, {withCredentials: true}).then((response) => {
                    resolve(response.data)
                }, (authError) => { reject(authError) })
            }
        })
    }
}
