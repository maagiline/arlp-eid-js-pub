type MobileIdSignInit = (lang: string, containerId: number, ssn: string, phone: string) => Promise<string>
export default MobileIdSignInit
