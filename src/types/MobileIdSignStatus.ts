type MobileIdSignStatus = (lang: string) => Promise<void>
export default MobileIdSignStatus
