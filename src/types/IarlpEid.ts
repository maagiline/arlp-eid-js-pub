import IidCardGetPerson from "./IidCardGetPerson";
import IidCardSign from "./IidCardSign";
import MobileIdAuthInit from "./MobileIdAuthInit";
import MobileIdGetPerson from "./MobileIdGetPerson";
import MobileIdSignInit from "./MobileIdSignInit";
import MobileIdSignStatus from "./MobileIdSignStatus";

export default interface IarlpEid {
    /**
     * Methods for determining the real identity of the user
     * */
    authenticate: {
        idCard: {
            /**
             * Returns data about the person and a hmac signed by the ArlpEID
             * service. hmac must be checked in ARLP to ensure that
             * information comes from ArlpEID service.
             * */
            getPerson: IidCardGetPerson
        }
        mobileId: {
            /**
             * Returns data about the person and a hmac signed by the ArlpEID
             * service. hmac must be checked in ARLP to ensure that
             * information comes from ArlpEID service.
             * */
            initiate: MobileIdAuthInit
            /**
             * Should be called after initiate has returned the control number.
             * Will return person's identity when authentication is done.
             * Includes hmac signed by the ArlpEID service. hmac must
             * be checked in ARLP to ensure that the information
             * comes from ArlpEID service.
             * */
            getPerson: MobileIdGetPerson
        }
    }

    /**
     * Methods for giving a legally binding signature to a document.
     * */
    sign: {
        idCard: {
            /**
             * Sign a document. Supply the ID that the document has in the
             * ARLP system. If promise resolves, signing was successful.
             *
             * Behind the scenes:
             * - ArlpEid service requests document's container from ARLP
             * - ArlpEid adds signature to container
             * - ArlpEid sends updated container to ARLP, along with the signee's social security number
             *
             * Along these steps, this plugin communicates with the
             * user's browser to get information from their ID
             * card, which is connected to their device.
             * */
            sign: IidCardSign
        }
        mobileId: {
            /**
             * Returns a control number that must be displayed to
             * the user while signing is ongoing (in practice,
             * it usually takes about 30 seconds)
             *
             * To sign via Mobile ID, the person must supply their
             * social security number and their phone number.
             * */
            initiate: MobileIdSignInit
            /**
             * Should be called after initiate has returned the control number.
             * Will return person's identity when authentication is
             * done. If promise resolves, signing was successful.
             *
             * Behind the scenes: refer to comments of ArlpEid.sign.idCard.sign
             * */
            checkStatus: MobileIdSignStatus
        }
    }
}
