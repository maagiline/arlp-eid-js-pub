type IidCardSign = (lang: string, containerId: number) => Promise<void>
export default IidCardSign
