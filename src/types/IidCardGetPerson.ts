import IpersonData from "./IpersonData";

type IidCardGetPerson = (lang: string) => Promise<IpersonData>
export default IidCardGetPerson
