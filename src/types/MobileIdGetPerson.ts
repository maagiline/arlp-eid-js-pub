import IpersonData from "./IpersonData";

type MobileIdGetPerson = (lang: string) => Promise<IpersonData>
export default MobileIdGetPerson
