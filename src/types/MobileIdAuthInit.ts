type MobileIdAuthInit = (lang: string, ssn: string, phone: string) => Promise<string>
export default MobileIdAuthInit
