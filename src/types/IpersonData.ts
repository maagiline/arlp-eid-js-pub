/**
 * The data structure returned by back-end when
 * authenticating via ID card or via Mobile ID
 * */
export default interface IpersonData {
    firstName: string,
    lastName: string,
    socialSecurityNumber: string,
    timestamp: number,
    hmac: string
}
