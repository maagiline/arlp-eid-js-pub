import axios from 'axios'
import IpersonData from './types/IpersonData'
import MobileIdAuthInit from './types/MobileIdAuthInit'
import MobileIdGetPerson from './types/MobileIdGetPerson'
import MobileIdSignInit from './types/MobileIdSignInit'
import MobileIdSignStatus from './types/MobileIdSignStatus'

interface IMobileId {
    authInit: MobileIdAuthInit
    getPerson: MobileIdGetPerson
    signInit: MobileIdSignInit
    signCheckStatus: MobileIdSignStatus
}

export class MobileId implements IMobileId {
    private serviceUrl: string
    private overrideMocking: boolean

    constructor(serviceUrl: string, overrideMocking: boolean = false) {
        this.serviceUrl = serviceUrl
        this.overrideMocking = overrideMocking
    }

    public authInit(lang: string, ssn: string, phone: string): Promise<string> {
        return new Promise((resolve, reject) => {
            const reqUrl = `${this.serviceUrl}/authenticate/mobile-id/initiate`
            const params = { lang, ssn, phone, overrideMocking: this.overrideMocking }

// Notify dev of possibility to specify returned user
            // tslint-disable-next-line no-console
            console.log(`Started mobile ID authentication in demo mode
                [${lang}][${ssn}][${phone}]. To
                specify which identity should be returned, make a
                request to ${this.serviceUrl}/demo/authenticatable
                with params firstName, lastName, ssn`)

            axios.post(reqUrl, params, {withCredentials: true})
                .then(response => {
                    resolve(response.data.challengeId)
                }, (error) => reject(error))
        })
    }

    public getPerson(lang: string): Promise<IpersonData> {
        return new Promise((resolve, reject) => {
            let reqUrl = `${this.serviceUrl}/authenticate/mobile-id/check`
            const params = { lang, overrideMocking: this.overrideMocking }

            axios.post(reqUrl, params, {withCredentials: true})
                .then((response) => {
                    if (response.data.finished) resolve(response.data)
                    else setTimeout(() => this.getPerson(lang).then((responseData) => resolve(responseData)), 1000)
                }, (error) => reject(error))
        })
    }

    public signInit(lang: string, containerId: number, ssn: string, phone: string): Promise<string> {
        return new Promise((resolve, reject) => {
            const reqUrl = `${this.serviceUrl}/sign/mobile-id/initiate`
            const params = { lang, containerId, ssn, phone, overrideMocking: this.overrideMocking }

            // Notify dev of possibility to specify returned user
            console.log(`Started mobile ID signing in demo mode
                [${lang}][${containerId}][${ssn}][${phone}]. To
                specify which identity should be returned, make a
                request to ${this.serviceUrl}/demo/authenticatable
                with params firstName, lastName, ssn`)

            axios.post(reqUrl, params, {withCredentials: true})
                .then((response) => {
                    resolve(response.data.challengeId)
                }, (error) => reject(error))
        })
    }

    public signCheckStatus(lang: string): Promise<void> {
        return new Promise((resolve, reject) => {
            const reqUrl = `${this.serviceUrl}/sign/mobile-id/check`
            const params = { lang, overrideMocking: this.overrideMocking }

            axios.post(reqUrl, params, {withCredentials: true}).then((response) => {
                if (response.data.finished) {
                    resolve(response.data)
                } else {
                    setTimeout(() => this.signCheckStatus(lang).then((responseData) => resolve(responseData)), 1000)
                }
            }, (error) => reject(error))
        })
    }
}
