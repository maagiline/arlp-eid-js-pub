import { IdCard } from './IdCard'
import { MobileId } from './MobileId'
import IarlpEid from './types/IarlpEid'
import IidCardGetPerson from './types/IidCardGetPerson'
import IidCardSign from './types/IidCardSign'
import MobileIdAuthInit from './types/MobileIdAuthInit'
import MobileIdGetPerson from './types/MobileIdGetPerson'
import MobileIdSignInit from './types/MobileIdSignInit'
import MobileIdSignStatus from './types/MobileIdSignStatus'

export default class implements IarlpEid {
    private serviceUrl: string
    private overrideMocking: boolean
    private idCard: IdCard
    private mobileId: MobileId

    constructor(serviceUrl: string, overrideMocking: boolean = false) {
        if (!serviceUrl) {
            throw new Error('serviceUrl must be provided to invoke ArlpEid')
        }

        // Remove trailing slash
        this.serviceUrl = serviceUrl.replace(/\/$/, '')
        this.overrideMocking = overrideMocking
        this.idCard = new IdCard(this.serviceUrl, this.overrideMocking)
        this.mobileId = new MobileId(this.serviceUrl, this.overrideMocking)

        this.authenticate = {
            idCard: {
                getPerson: (...args) => this.idCard.getPerson(...args),
            },
            mobileId: {
                getPerson: (...args) => this.mobileId.getPerson(...args),
                initiate: (...args) => this.mobileId.authInit(...args),
            },
        }

        this.sign = {
            idCard: {
                sign: (...args) => this.idCard.sign(...args),
            },
            mobileId: {
                checkStatus: (...args) => this.mobileId.signCheckStatus(...args),
                initiate: (...args) => this.mobileId.signInit(...args),
            },
        }
    }

    public authenticate: {
        idCard: {
            getPerson: IidCardGetPerson
        },
        mobileId: {
            initiate: MobileIdAuthInit,
            getPerson: MobileIdGetPerson
        }
    }

    public sign: {
        idCard: {
            sign: IidCardSign
        },
        mobileId: {
            initiate: MobileIdSignInit,
            checkStatus: MobileIdSignStatus
        }
    }
}
