export default (milliseconds: number) => {
    return new Promise((resolve: () => void) => {
        setTimeout(resolve, milliseconds)
    })
}
